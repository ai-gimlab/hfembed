package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/sysUtils/go-libraries/libhelp"
	"gitlab.com/sysUtils/go-libraries/libopt"

	"github.com/pborman/getopt/v2"
)

func main() {
	var (
		logger     *log.Logger = log.New(os.Stderr, logPrefix, log.Lshortfile)
		bodyString string
	)

	// --- check user input chars --- //
	// command name
	if err := libopt.ChkChars(chrLowCase+chrUpCase+chrNums+chrCmdName, os.Args[:1]); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// check short/long options for single/double dash
	if len(os.Args) > 1 && os.Args[1] != "" && os.Args[len(os.Args)-1] != "" {
		if err := checkLongOptionDashes(os.Args[1:]); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}

	// --- check cmdline options --- //
	// any error during parse
	if err := getopt.Getopt(nil); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// check if string arguments was provided
	if err := argProvided(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// check for flag dependencies
	if err := checkFlagDependencies(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// informative flags
	switch {
	case flagHelp:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagHelpLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		libhelp.PrintHelp(helpMsgUsage, helpMsgBody, helpMsgTips, true)
		os.Exit(0)
	case flagVersion:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagVersionLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		libhelp.PrintVersion(verMsg)
		os.Exit(0)
	case flagListModels:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagListModelsLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		fmt.Printf("%s\n", testedModels)
		os.Exit(0)
	}

	// check format of 'string' type flags
	if err := checkFlagTypeString(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// - check non-options arguments: prompt message - //
	nonOptArgs := getopt.Args()
	switch {
	case len(nonOptArgs) == 0 || (len(nonOptArgs) > 0 && nonOptArgs[0] == ""):
		// no PROMPT given
		logger.Fatal(fmt.Errorf("\a%s\n%s", "missing user PROMPT", tryMsg))
	case len(nonOptArgs) > 1:
		// to many PROMPTs
		var s string
		for i, v := range nonOptArgs {
			if i == 0 {
				s += fmt.Sprintf("[PROMPT %d]\n", i+1)
				s += fmt.Sprintf("%s <- PROBABLY THE SOURCE OF THE PROBLEM\n\n", v)
				continue
			} else {
				s += fmt.Sprintf("[PROMPT %d]\n", i+1)
				s += fmt.Sprintf("%s\n\n", v)
			}
		}
		logger.Fatal(fmt.Errorf("\a%s:\n\n%s\n%s", "too many user PROMPTs or unknown option", s, tryMsg))
	default:
		prompt = nonOptArgs[0]
		// sanitize prompt
		prompt = strings.ReplaceAll(prompt, "\\n", "\n")
	}

	// --- config program --- //
	// parameters
	if err := configFlags(); err != nil {
		switch {
		case err.Error() == "EXIT":
			os.Exit(-1)
		default:
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}

	// create JSON payload
	payloadJSON, err := createPayloadJSON()
	if err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// preview and exit
	if flagPreview {
		ppJSON, err := prettyPrintJsonString(string(payloadJSON))
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		fmt.Printf("model: %s\n\n", model)
		fmt.Printf("payload:\n%s\n", ppJSON)
		os.Exit(0)
	}

	// --- execute request and print result --- //
	// make HTTP request (single response mode or stream mode)
	for i := -1; i < retries; i++ {
		resp, err := httpRequestPOST(payloadJSON)
		if err != nil {
			switch {
			case strings.HasSuffix(err.Error(), timeoutErr) && i < (retries-1):
				// manage timeout error and retries
				fmt.Printf("connection timeout (%.0f sec): retry in %d sec. ... attempt %d of %d\n", timeout.Seconds(), retriesWait, i+2, retries)
				time.Sleep(time.Second * time.Duration(retriesWait))
				continue
			case (strings.Contains(err.Error(), modelErrLoadingCode) && strings.Contains(err.Error(), modelErrLoadingString)) && i < (retries-1):
				// manage ''model is loading' error and retries
				fmt.Printf("model %q is currently loading: retry in %d sec. ... attempt %d of %d\n", model, retriesWait, i+2, retries)
				time.Sleep(time.Second * time.Duration(retriesWait))
				continue
			default:
				// manage all errors, including 'last timeout retry'
				if i > -1 {
					// print an empty line after all timeout messages, if any
					fmt.Println()
				}
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
		}
		// no errors, no timeout: read body, break loop, continue execution
		if i > -1 {
			// print an empty line after all timeout messages, if any
			fmt.Println()
		}
		bodyString = resp
		break
	}

	// --- parse response --- //
	// raw response
	if flagRaw {
		fmt.Printf("%s\n", bodyString)
		os.Exit(0)
	}
	// json response
	if flagJson {
		ppJSON, err := prettyPrintJsonString(bodyString)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		fmt.Printf("%s\n", ppJSON)
		os.Exit(0)
	}

	// extract and save embeddings
	if err := embeddingsCSV(bodyString); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}
}
