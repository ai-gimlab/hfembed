package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	"os"
	"strings"

	"github.com/joho/godotenv"
)

// create and send HTTP request
func httpRequestPOST(payloadJSON []byte) (bodyString string, err error) {
	// function name to return if any error occur
	var funcName string = "httpRequestPOST"

	// load env configs
	key, err := loadEnv()
	if err != nil {
		return "", err
	}

	// get api key
	var bearer string = "Bearer " + key
	var endpoint string

	endpoint = urlEmbed + model

	endpoint = "https://httpbin.org/post"

	// create and configure HTTP request
	req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer(payloadJSON))
	if err != nil {
		return "", fmt.Errorf("func %q - error creating HTTP request: %w\n", funcName, err)
	}

	// set HTTP User-Agent Header
	ua, err := createUserAgent(req)
	if err != nil {
		return "", fmt.Errorf("func %q - %w\n", funcName, err)
	}

	// add the necessary HTTP headers
	req.Header.Add("Authorization", bearer)
	req.Header.Add("Content-Type", "application/json; charset=UTF-8")
	req.Header.Add("User-Agent", ua)

	// create HTTP client
	client := &http.Client{
		Timeout: timeout,
	}

	// make HTTP request
	resp, errResp := client.Do(req)
	if errResp != nil {
		e := strings.ReplaceAll(errResp.Error(), "\"", "'")
		return "", fmt.Errorf("func '%s' - error during POST request: %s\n", funcName, e)
	}
	defer resp.Body.Close()

	// check HTTP response status
	if resp.StatusCode != http.StatusOK {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return "", fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
		}
		ppJSON, err := prettyPrintJsonString(string(body))
		errDetails := fmt.Sprintf("// ------ ERROR DETAILS ------ //\n%s", ppJSON)

		return "", fmt.Errorf("func %q - error during HTTP transaction: %s\n\n%s\n", funcName, resp.Status, errDetails)
	}

	// parse HTTP response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
	}

	return string(body), nil
}

// load API key
func loadEnv() (key string, err error) {
	// function name to return if any error occur
	var funcName string = "loadEnv"

	// get env
	if flagFile != "" {
		// from supplied .env file
		err := godotenv.Load(flagFile)
		if err != nil {
			return "", fmt.Errorf("func %q - error loading env file %s", funcName, flagFile)
		}
	} else {
		// from os environment
		envVar := os.Getenv(env)

		// from default .env file
		cfgEnv := os.Getenv(cfgRootDir) + cfgDir + cfgFile
		errFile := godotenv.Load(cfgEnv)

		// check result
		if errFile != nil && envVar == "" {
			return "", fmt.Errorf("func %q - error loading default env file or missing env var %q", funcName, env)
		}
	}

	// load env configs
	key, ok := os.LookupEnv(env)
	if !ok || key == "" {
		return "", fmt.Errorf("func %q - Hugging Face API KEY not given", funcName)
	}
	return key, nil
}

// createUserAgent return a custom 'User-Agent'
func createUserAgent(req *http.Request) (ua string, err error) {
	// function name to return if any error occur
	var funcName string = "createUserAgent"

	// load default Go User-Agent
	dump, err := httputil.DumpRequestOut(req, true)
	if err != nil {
		return "", fmt.Errorf("func %q - error retrieving Go default User-Agent: %w\n", funcName, err)
	}
	_, d, _ := strings.Cut(string(dump), "User-Agent: ")
	d, _, _ = strings.Cut(d, "\r\n")

	// create custom UA
	ua = fmt.Sprintf("%s (%s)", customUA, d)

	return ua, nil
}
