package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func embeddingsCSV(re string) error {
	// function name to return if any error occur
	var funcName string = "embeddingsCSV"

	// last row id of existing csv file - used for append data
	var embedCsvFileOffset int = 0

	// Define a variable to store the parsed data
	var vectors [][]float64

	// create or open file (append)
	f, err := os.OpenFile(embedCsvFile, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0600)
	if err != nil {
		return fmt.Errorf("func %q - error creating embeddings csv file: %w\n", funcName, err)
	}
	defer f.Close()

	// check file size
	fileInfo, err := f.Stat()
	if err != nil {
		return fmt.Errorf("func %q - error checking csv file size: %w\n", funcName, err)
	}

	// Create a CSV writer
	csvWriter := csv.NewWriter(f)

	// Create a CSV reader
	csvReader := csv.NewReader(f)

	// if new file write headers, else check last row id
	switch {
	case fileInfo.Size() == 0:
		// if csv file does not exist, or is empty, write headers
		if err := csvWriter.Write(embedCsvHeader); err != nil {
			return fmt.Errorf("func %q - error writing headers to csv file %s: %w\n", funcName, embedCsvFile, err)
		}
	case fileInfo.Size() != 0:
		var records [][]string
		for {
			record, err := csvReader.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				return fmt.Errorf("func %q - error reading csv file %s: %w\n", funcName, embedCsvFile, err)
			}

			records = append(records, record)
		}
		if offset, err := strconv.ParseInt(records[len(records)-1][0], 10, 64); err != nil {
			return fmt.Errorf("func %q - error reading last line of csv file %s: %w\n", funcName, embedCsvFile, err)
		} else {
			embedCsvFileOffset = int(offset) + 1
		}
	}

	// Unmarshal the JSON string into the data variable
	err = json.Unmarshal([]byte(re), &vectors)
	if err != nil {
		return fmt.Errorf("func %q - error decoding http response data to json: %w\n", funcName, err)
	}

	// write new embeddings
	for i, v := range vectors {
		var embedding strings.Builder
		id := fmt.Sprintf("%d", embedCsvFileOffset)
		text := inputs[i]
		if _, err := embedding.WriteString("["); err != nil {
			return fmt.Errorf("func %q - error creating embeddings CSV row: %w\n", funcName, err)
		}
		for i, e := range v {
			f := strconv.FormatFloat(e, 'f', -1, 64)
			trimmed := strings.TrimRight(strings.TrimRight(f, "0"), ".")
			if _, err := embedding.WriteString(trimmed); err != nil {
				return fmt.Errorf("func %q - error creating embeddings CSV field: %w\n", funcName, err)
			}
			if i < len(v)-1 {
				if _, err := embedding.WriteRune(','); err != nil {
					return fmt.Errorf("func %q - error creating embeddings CSV field: %w\n", funcName, err)
				}
			}
		}
		if _, err := embedding.WriteRune(']'); err != nil {
			return fmt.Errorf("func %q - error creating embeddings CSV row: %w\n", funcName, err)
		}

		// Write the data record to the CSV file.
		if err := csvWriter.Write([]string{id, text, embedding.String()}); err != nil {
			return fmt.Errorf("func %q - error writing data to CSV file %s: %w\n", funcName, embedCsvFile, err)
		}
		embedCsvFileOffset++
	}

	// Flush the CSV writer.
	csvWriter.Flush()

	fmt.Printf("\nembeddings data successfully written to file %q\n\n", embedCsvFile)

	return nil
}
