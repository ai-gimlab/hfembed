# hfembed

## Table of Contents

- [Introduction](#introduction)
- [Disclaimer](#disclaimer)
- [Getting started](#getting-started)
  - [Binary](#binary)
  - [Compile from source](#compile-from-source)
  - [Default settings](#default-settings)
- [Usage example](#usage-example)
- [Models tested](#models-tested)
- [How to get and use Hugging Face Api Key](#how-to-get-and-use-hugging-face-api-key)
- [Credits](#credits)

---

## Introduction

hfembed: Terminal embedding client for [Hugging Face's](https://huggingface.co/models?library=sentence-transformers) embedding models written in [Go](https://go.dev/).

Quickly get text embeddings to gain insights into the semantic meaning and relationships between text elements.

Return a cvs table with columns `id, text, embedding`.

By default save table to file *data.csv*, in current folder. To save to a different file use `--csv FILENAME` option. If csv file already exist, append data, with a new record `id`.

With the options `--response-json` or `--response-raw`, embeddings are not saved to file. Instead, the JSON data is output to stdout.

Can be used with companion utility [eucos](https://gitlab.com/ai-gimlab/eucos#eucos).

The man page can be found [here](https://gitlab.com/ai-gimlab/hfembed/-/tree/main/man).

In this guide we use the Bash shell on Linux systems, but *hfembed* can run on other platforms as well.

To follow this guide a basic knowledge of what [embeddings](https://huggingface.co/blog/getting-started-with-embeddings) are is required.

---

[top](#table-of-contents)

---

## Disclaimer

I enjoy to develop this utilities just for my personal use. So, use them at your own risk.

---

[top](#table-of-contents)

---

## Getting started

### Binary

Prebuilt binary packages for Linux, Windows and MacOS can be downloaded from [here](https://gitlab.com/ai-gimlab/hfembed/-/releases).

---

### Compile from source

If you prefer, clone this repo and compile from sources.

Prerequisite: [Go Development Environment](https://go.dev/dl/) installed.

Clone this repo and build:

```bash
git clone https://gitlab.com/ai-gimlab/hfembed.git
cd hfembed
go mod init hfembed && go mod tidy
go build .
```

---

### Default settings

- **model**: sentence-transformers/all-MiniLM-L6-v2
- **connection timeout**: 120 sec
- **number of retries**: 0
- **wait before retry**: 10 sec
- **embeddings csv filename**: data.csv

---

[top](#table-of-contents)

---

## Usage Example

Basically, a prompt is enough:

```bash
hfembed "PROMPT"
```

PROMPT must always be enclosed in "double" quotes. Multiple PROMPTs can be passed, separated by double commas:

```bash
hfembed "PROMPT_1,,PROMPT_2,,...,,PROMPT_N"
```

---

Let's try a basic example. We get embedding representations of the following sentences:

1. *Change is impossible, claimed the ancient Greek philosopher Parmenides*
2. *Saturn's ring system is categorized into several main groups*
3. *the term Galilean relativity refer to a somewhat modernized version of Galileo's principle*

We also get embedding representations of the sentence *In physics, what is meant by Galilean relativity?* and use this embedding to check the similarity against the previous three sentences.

Add all sentences to file *sentences.txt*:

```text
export S0="Change is impossible, claimed the ancient Greek philosopher Parmenides"
export S1="Saturn's ring system is categorized into several main groups"
export S2="the term Galilean relativity refer to a somewhat modernized version of Galileo's principle"
export S3="In physics, what is meant by Galilean relativity?"
```

Import all new variables, `$S0`, `$S1`, `$S2` and `$S3`, into Bash environment with command `source`:

```bash
source sentences.txt
```

Because we need to pass multiple PROMPTs to *hfembed* command, we must use double commas as a sentences separator. Enclose PROMPTs in double quotes:

```bash
# actual query to AI model - get embeddings
hfembed "$S0,,$S1,,$S2,,$S3"
```

By default embeddings are saved in file *data.csv* (an excerpt):

```csv
id,text,embedding
0,"Change is impossible, claimed the ancient Greek philosopher Parmenides",[-0.2947986423969269,-0.17843618988990784,-0.060902781784534454,...,-0.04022528976202011]"
1,Saturn's ring system is categorized into several main groups,"[[0.0643889382481575,-0.13575352728366852,-0.056705277413129807,...,]"
2,the term Galilean relativity refer to a somewhat modernized version of Galileo's principle,"[-0.28777095675468445,-0.07869274169206619,-0.06742411851882935,...,-0.005569660570472479]"
3,In physics, what is meant by Galilean relativity?,"[-0.015032283030450344,-0.14954693615436554,-0.01659010536968708,...,-0.24845945835113525]"
```

The following python code compare the similarity of the embedding of question *In physics, what is meant by Galilean relativity?*, the last row of csv file, with the embeddings of the other sentences:

```python
import pandas as pd
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from ast import literal_eval

# Hardcoded filename
filename = 'data.csv'

# Load CSV file
df = pd.read_csv(filename, converters={'embedding': literal_eval})

# Convert 'embedding' column from object to ndarray
df['embedding'] = df['embedding'].apply(np.array)

# Extract reference sentence embedding
reference_embedding = df.iloc[-1]['embedding']

# Compute cosine similarity between all embedded sentences and the reference sentence
cosine_similarities = df.iloc[:-1]['embedding'].apply(lambda x: cosine_similarity([x], [reference_embedding])[0][0])

# Output results
print("[reference sentence]")
print(df.iloc[-1]['text'])
print("\n[cosine similarity]")

for index, similarity in enumerate(cosine_similarities):
    print(f"{df.iloc[index]['text']}: {similarity}")
```

---

Results:

---

**reference sentence**: *In physics, what is meant by Galilean relativity?*

**similarity**:

| sentence | similarity value |
| --- | --- |
| the term Galilean relativity refer to a somewhat modernized version of Galileo's principle | 0.8387035664387487 |
| Change is impossible, claimed the ancient Greek philosopher Parmenides | 0.1975719641200649 |
| Saturn's ring system is categorized into several main groups | 0.18950822837476974 |

Embedding of sentence *the term Galilean relativity refer to a somewhat modernized version of Galileo's principle* has the highest cosine similarity value, **0.8387035664387487**, or it has the highest semantic similarity with the embedding of sentence *In physics, what is meant by Galilean relativity?*, as expected.

---

For **cosine similarity** or **euclidean distance** can also be used the companion utility [eucos](https://gitlab.com/ai-gimlab/eucos#eucos).

---

[top](#table-of-contents)

---

## Models tested

To list all tested models use `--list-models` option:

```bash
# actual query
hfembed --list-models
```

Output:

```text
tested models:
  intfloat/multilingual-e5-large
  sentence-transformers/paraphrase-multilingual-MiniLM-L12-v2
  sentence-transformers/all-MiniLM-L6-v2 (default)
  sentence-transformers/LaBSE
  BAAI/bge-m3

ref: https://huggingface.co/models?library=sentence-transformers
```

---

To use other models go to a given model page, then copy model name. As an example we use [sentence-transformers/all-mpnet-base-v2](https://huggingface.co/sentence-transformers/all-mpnet-base-v2) model.

To copy model name click on `copy` icon on the right of model name, as shown in the picture below:

![model name](images/modelsSelectionEdit.png)

Then use the copied name as *MODELNAME* for the `--model MODELNAME` option, such as:

```bash
# example query
hfembed --model "sentence-transformers/all-mpnet-base-v2" PROMPT
```

---

Use `--help` to view all options:

```bash
hfembed --help
```


Output:

```text
Usage: hfembed [OPTIONS] "PROMPT"

Terminal embedding client for Hugging Face's embedding models

Defaults:
  model: sentence-transformers/all-MiniLM-L6-v2
  connection timeout: 120 sec
  number of retries: 0
  wait before retry: 10 sec
  embeddings csv filename: data.csv

Order matter:
  first OPTIONS
  then PROMPT

Notes:
  . return a cvs table with columns 'id, text, embedding'
    append table to file 'data.csv'
    to save/append in a different file use '--csv FILENAME' option
  . PROMPT must always be enclosed in "double" quotes.
  . multiple PROMPTs can be passed, separated by double commas
    eg: hfembed "PROMPT_1,,PROMPT_2,,...,,PROMPT_N"

Online Documentation:
  <https://gitlab.com/ai-gimlab/hfembed#hfembed>

OPTIONS:
      --csv=CSV_FILE              save/append embeddings to CSV_FILE insted of default 'data.csv'
  -f, --file=APIKEY_FILE          file with Hugging Face api key (HF_API_KEY=your_apikey)
  -l, --list-models               lists the names of the tested models and exit
  -m, --model=MODEL               select model
                                  use '--list-models' to list tested models
  -p, --preview                   preview request payload and exit
      --response-raw              print full response body raw
                                  response is not saved to csv file
      --response-json             print full response body json formatted
                                  response is not saved to csv file
      --retries=RETRIES           number of connection retries if any timeout network error occur
                                  use the '--retries-wait' option to insert a pause between retries
      --retries-wait=SECONDS      in conjunction with '--retries RETRIES' option insert a pause between retries
      --timeout=SECONDS           network connection timeout, in seconds
                                  apply to a complete request/response session
                                  SECONDS = 0 means no timeout
      --help                      print this help
      --version                   output version information and exit

  💡                              hfembed "PROMPT_1,,PROMPT_2,,...,,PROMPT_N"
```

[top](#table-of-contents)

---

## How to get and use Hugging Face Api Key

Get your api key from [Hugging Face](https://huggingface.co/join) site.

There are three way to supply api key:

**[1] default**

Create file `.env` and insert the following line:

```bash
HF_API_KEY='hf-YOUR-API-KEY'
```

Copy file `.env` to `$HOME/.local/etc/` folder.

**[2] environment variable**

If you prefer, export HF_API_KEY as environment variable:

```bash
export HF_API_KEY='hf-YOUR-API-KEY'
```

**[3] use '-f, --file' option**

You can also supply your own key file, containing the statement `HF_API_KEY='hf-YOUR-API-KEY'`, and pass its path as argument to `-f, --file` option.

---

[top](#table-of-contents)

---

## Credits

This project is made possible thanks to the use of the following libraries and the precious work of those who create and maintain them.
Of course thanks also to all those who create and maintain the AI models.

- [godotenv](https://github.com/joho/godotenv)
- [getopt](https://github.com/pborman/getopt/)
- [models](https://huggingface.co/models?library=sentence-transformers)
- [Hugging Face Hub](https://huggingface.co/)

---

[top](#table-of-contents)

---

[Others gimlab repos](https://gitlab.com/users/gimaldi/groups)

---
