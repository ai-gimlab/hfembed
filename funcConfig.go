package main

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"time"

	"github.com/pborman/getopt/v2"
)

func configFlags() error {
	// function name to return if any error occur
	var funcName string = "configFlags"

	if getopt.GetCount(flagCsvLong) > 0 && !flagPreview {
		// if csv file does not exist ask permission to create a new one
		var r rune
		_, errFileNotExist := os.Open(flagCsv)
		if errors.Is(errFileNotExist, fs.ErrNotExist) {
			// ask to confirm new file creation
			fmt.Printf("File %q does not exist. Create new one (%q to quit and exit)? [y/n/q]: ", flagCsv, "q")
			if _, err := fmt.Scanf("%c", &r); err != nil {
				return fmt.Errorf("func %q - error creating CSV file %q: %w", funcName, flagCsv, err)
			}
			switch {
			case r == 'y' || r == 'Y':
				embedCsvFile = flagCsv
			case r == 'n' || r == 'N':
				flagCsv = ""
			case r == 'q' || r == 'Q':
				return (fmt.Errorf("EXIT"))
			default:
				return fmt.Errorf("wrong option: %c\nexit program", r)
			}
		}
		embedCsvFile = flagCsv
	}
	if getopt.GetCount(flagTimeoutLong) > 0 {
		timeout = time.Second * time.Duration(flagTimeout)
	}
	if getopt.GetCount(flagRetriesLong) > 0 {
		retries = flagRetries
	}
	if getopt.GetCount(flagRetriesWaitLong) > 0 {
		retriesWait = flagRetriesWait
	}
	if flagModel != "" {
		model = flagModel
	}
	return nil
}
