# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.0](https://gitlab.com/ai-gimlab/hfembed/-/releases/v1.2.0) - 2024-06-20

### Added

- Custom User-Agent for request header.

## [1.1.0](https://gitlab.com/ai-gimlab/hfembed/-/releases/v1.1.0) - 2024-02-20

### Changed

- Changed default model.

## [1.0.0](https://gitlab.com/ai-gimlab/hfembed/-/releases/v1.0.0) - 2024-01-23

### First stable release

---

[Back to project main page](https://gitlab.com/ai-gimlab/hfembed#hfembed)

---
