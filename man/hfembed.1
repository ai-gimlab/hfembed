.TH HFEMBED 1 "August 2024" "hfembed" "User Commands"
.SH NAME
hfembed \- Terminal embedding client for Hugging Face's embedding models
.SH SYNOPSIS
\fBhfembed\fR [\fIOPTIONS...\fR] "\fIPROMPT\fR"
.SH DESCRIPTION
\fBhfembed\fR is a command-line tool that allows users to obtain embeddings for given prompts using Hugging Face's embedding models. It provides a convenient way to generate embeddings and save them in a CSV file.

The command requires a mandatory prompt enclosed in double quotes. Multiple prompts can be passed, separated by double commas.

By default, the embeddings are appended to a CSV file named 'data.csv'. This file contains three columns: 'id', 'text', and 'embedding'. The 'id' column represents a unique identifier for each prompt, the 'text' column contains the prompt itself, and the 'embedding' column contains the generated embedding.

Users can customize the output CSV file using the \fB\-\-csv\fR option. Additionally, various options are available to control the behavior of the command, such as selecting a specific model, setting connection timeout and retries, and previewing the request payload.

.SH OPTIONS
.TP
\fB\-\-csv=CSV_FILE\fR
Specify the CSV file to save/append embeddings to instead of the default 'data.csv'.
.TP
\fB\-f, \-\-file=APIKEY_FILE\fR
Specify the file containing the Hugging Face API key (HF_API_KEY=your_apikey).
.TP
\fB\-l, \-\-list-models\fR
List the names of the tested models and exit.
.TP
\fB\-m, \-\-model=MODEL\fR
Select the model to use for generating embeddings. Use '--list-models' to list the available models.
.TP
\fB\-p, \-\-preview\fR
Preview the request payload and exit.
.TP
\fB\-\-response-raw\fR
Print the full response body in raw format. The response is not saved to the CSV file.
.TP
\fB\-\-response-json\fR
Print the full response body in JSON format. The response is not saved to the CSV file.
.TP
\fB\-\-retries=RETRIES\fR
Specify the number of connection retries if any timeout network error occurs. Use the '--retries-wait' option to insert a pause between retries.
.TP
\fB\-\-retries-wait=SECONDS\fR
In conjunction with the '--retries RETRIES' option, insert a pause of SECONDS between retries.
.TP
\fB\-\-timeout=SECONDS\fR
Set the network connection timeout in seconds. Applies to a complete request/response session. SECONDS = 0 means no timeout.
.TP
\fB\-\-help\fR
Print the help message.
.TP
\fB\-\-version\fR
Output version information and exit.

.SH EXAMPLES
Generate embeddings for a single prompt and save them to the default 'data.csv' file:

.PP
.in +7n
.EX
hfembed "This is a test prompt."
.EE
.in

Generate embeddings for multiple prompts and save them to a specified CSV file:

.PP
.in +7n
.EX
hfembed --csv embeddings.csv "Prompt 1,,Prompt 2,,Prompt 3"
.EE
.in

List the available models:

.PP
.in +7n
.EX
hfembed --list-models
.EE
.in

.SH ONLINE DOCUMENTATION
For more detailed information, visit: <https://gitlab.com/ai-gimlab/hfembed#hfembed>

.SH AUTHOR
Written by ai-gimlab.

.SH BUGS
Report bugs to <https://gitlab.com/ai-gimlab/hfembed/-/issues>

.SH COPYRIGHT
Copyright (C) 2023 ai-gimlab. License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
.br
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

.SH "SEE ALSO"
.BR curl (1),
.BR jq (1),
.BR python (1)
