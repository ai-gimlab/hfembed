package main

import (
	"time"

	"gitlab.com/sysUtils/go-libraries/libhelp"
	"gitlab.com/sysUtils/go-libraries/libopt"
)

// --- embeddings structs --- //
// request //
type requestEmbed struct {
	Inputs []string `json:"inputs"`
}

// --- end structs --- //

const (
	// .env file default location
	cfgRootDir string = "HOME" // used as environ var
	cfgDir     string = "/.local/etc/"
	cfgFile    string = ".env"

	// permitted cmdline chars
	chrLowCase  string = libopt.ChrLowCase  // a, b, c, d, ...
	chrUpCase   string = libopt.ChrUpCase   // A, B, C, D, ...
	chrNums     string = libopt.ChrNums     // 0, 1, 2, 3, ...
	chrNumsPfix string = libopt.ChrNumsPfix // hexadecimal and binary number prefix (eg: 0x or 0b)
	chrFlags    string = libopt.ChrFlags    // used in cmdline options (eg: -a -b -c) - NOTE: include space
	chrCmdName  string = libopt.ChrCmdName  // used for command name (eg: ./mycmd)
	chrExtra    string = libopt.ChrExtra    // for special cases (eg: email.name@email.address)
	chrPath     string = libopt.ChrPath

	// network settings
	customUA                string        = "hfembed/1.0"     // User-Agent
	timeoutCompletionSingle time.Duration = time.Second * 120 // network timeout
	connectionRetries       int           = 0                 // number of reconnection attempts in case of timeout
	connectionRetriesWait   int           = 10                // seconds to wait before any reconnection attempts
	timeoutErr              string        = "(Client.Timeout exceeded while awaiting headers)\n"

	// API config defaults
	env                          string        = "HF_API_KEY"
	urlModelsTechRef             string        = "https://huggingface.co/models?pipeline_tag=sentence-similarity&sort=trending"
	urlEmbed                     string        = "https://api-inference.huggingface.co/pipeline/feature-extraction/"
	defaultModel                 string        = "sentence-transformers/all-MiniLM-L6-v2"
	defaultTimeout               time.Duration = timeoutCompletionSingle
	defaultConnectionRetries     int           = connectionRetries
	defaultConnectionRetriesWait int           = connectionRetriesWait
	defaultEmbedCsvFile          string        = "data.csv"

	// other
	objectSplitChar       string = ",," // strings objects separator for sentences embedding
	modelErrLoadingCode   string = "503 Service Unavailable"
	modelErrLoadingString string = "currently loading\",\n  \"estimated_time\":"
)

var (
	// vars from custom libraries
	cmdName   string = libhelp.CmdName
	tryMsg    string = libhelp.TryMsg
	logPrefix string = libhelp.LogPrefix

	// API config vars
	prompt         string
	model          string        = defaultModel
	timeout        time.Duration = defaultTimeout
	retries        int           = defaultConnectionRetries
	retriesWait    int           = defaultConnectionRetriesWait
	inputs         []string      // text to embed
	embedCsvFile   string        = defaultEmbedCsvFile
	embedCsvHeader []string      = []string{"id", "text", "embedding"}
)
