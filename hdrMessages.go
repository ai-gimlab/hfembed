package main

const helpMsgUsage = "Usage: %s [OPTIONS] \"PROMPT\"\n"

const helpMsgBody = `
Terminal embedding client for Hugging Face's embedding models

Defaults:
  model: sentence-transformers/all-MiniLM-L6-v2
  connection timeout: 120 sec
  number of retries: 0
  wait before retry: 10 sec
  embeddings csv filename: data.csv

Order matter:
  first OPTIONS
  then PROMPT

Notes:
  . return a cvs table with columns 'id, text, embedding'
    append table to file 'data.csv'
    to save/append in a different file use '--csv FILENAME' option
  . PROMPT must always be enclosed in "double" quotes.
  . multiple PROMPTs can be passed, separated by double commas
    eg: hfembed "PROMPT_1,,PROMPT_2,,...,,PROMPT_N"

Online Documentation:
  <https://gitlab.com/ai-gimlab/hfembed#hfembed>

OPTIONS:
      --csv=CSV_FILE              save/append embeddings to CSV_FILE insted of default 'data.csv'
  -f, --file=APIKEY_FILE          file with Hugging Face api key (HF_API_KEY=your_apikey)
  -l, --list-models               lists the names of the tested models and exit
  -m, --model=MODEL               select model
                                  use '--list-models' to list tested models
  -p, --preview                   preview request payload and exit
      --response-raw              print full response body raw
                                  response is not saved to csv file
      --response-json             print full response body json formatted
                                  response is not saved to csv file
      --retries=RETRIES           number of connection retries if any timeout network error occur
                                  use the '--retries-wait' option to insert a pause between retries
      --retries-wait=SECONDS      in conjunction with '--retries RETRIES' option insert a pause between retries
      --timeout=SECONDS           network connection timeout, in seconds
                                  apply to a complete request/response session
                                  SECONDS = 0 means no timeout
      --help                      print this help
      --version                   output version information and exit
`

const helpMsgTips = "  💡                              %s \"PROMPT_1,,PROMPT_2,,...,,PROMPT_N\"\n"

// befor this var, always print cmd name, without newline char (os.Args[0]). eg: fmt.Fprintf(os.Stdout, "%s", os.Args[0]); fmt.Println(verMsg)
const verMsg = ` (gimlab) 1.2.0
Copyright (C) 2024 gimlab.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.`

const testedModels string = `tested models:
  intfloat/multilingual-e5-large
  sentence-transformers/paraphrase-multilingual-MiniLM-L12-v2
  sentence-transformers/all-MiniLM-L6-v2  (default)
  sentence-transformers/LaBSE
  BAAI/bge-m3

ref: https://huggingface.co/models?library=sentence-transformers`
