package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/pborman/getopt/v2"
)

// how many flags are used
const flagsCount int = 12

// flag labels
const (
	fakeRune             rune   = '#'
	flagHelpShort        rune   = fakeRune
	flagHelpLong         string = "help"
	flagVersionShort     rune   = fakeRune
	flagVersionLong      string = "version"
	flagFileShort        rune   = 'f'
	flagFileLong         string = "file"
	flagListModelsShort  rune   = 'l'
	flagListModelsLong   string = "list-models"
	flagModelShort       rune   = 'm'
	flagModelLong        string = "model"
	flagPreviewShort     rune   = 'p'
	flagPreviewLong      string = "preview"
	flagTimeoutShort     rune   = fakeRune
	flagTimeoutLong      string = "timeout"
	flagRetriesshort     rune   = fakeRune
	flagRetriesLong      string = "retries"
	flagRetriesWaitShort rune   = fakeRune
	flagRetriesWaitLong  string = "retries-wait"
	flagJsonShort        rune   = fakeRune
	flagJsonLong         string = "response-json"
	flagRawShort         rune   = fakeRune
	flagRawLong          string = "response-raw"
	flagCsvShort         rune   = fakeRune
	flagCsvLong          string = "csv"
)

// Declare flags and have getopt return pointers to the values.
var (
	flagHelp        bool
	flagVersion     bool
	flagFile        string
	flagListModels  bool
	flagModel       string
	flagPreview     bool
	flagTimeout     int64
	flagRetries     int
	flagRetriesWait int
	flagJson        bool
	flagRaw         bool
	flagCsv         string
	flags           []getopt.Option = make([]getopt.Option, 0, flagsCount)
	flagsCounter    map[string]int  = make(map[string]int)
	flagsTypes      []string        = make([]string, 0, flagsCount)
)

// init vars at program start with 'init()' internal function
func init() {
	flags = append(flags, getopt.FlagLong(&flagHelp, "help", '\U0001f595', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagHelp))
	flags = append(flags, getopt.FlagLong(&flagVersion, "version", '\U0001f6bd', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagVersion))
	flags = append(flags, getopt.FlagLong(&flagFile, flagFileLong, flagFileShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFile))
	flags = append(flags, getopt.FlagLong(&flagListModels, flagListModelsLong, flagListModelsShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagListModels))
	flags = append(flags, getopt.FlagLong(&flagModel, flagModelLong, flagModelShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagModel))
	flags = append(flags, getopt.FlagLong(&flagPreview, flagPreviewLong, flagPreviewShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPreview))
	flags = append(flags, getopt.FlagLong(&flagTimeout, flagTimeoutLong, '\U0001f521', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTimeout))
	flags = append(flags, getopt.FlagLong(&flagRetries, flagRetriesLong, '\U0001f523', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagRetries))
	flags = append(flags, getopt.FlagLong(&flagRetriesWait, flagRetriesWaitLong, '\U0001f423', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagRetriesWait))
	flags = append(flags, getopt.FlagLong(&flagJson, flagJsonLong, '\U0001f524', "").SetGroup("csv"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagJson))
	flags = append(flags, getopt.FlagLong(&flagRaw, flagRawLong, '\U0001f525', "").SetGroup("csv"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagRaw))
	flags = append(flags, getopt.FlagLong(&flagCsv, flagCsvLong, '\U0001f526', "").SetGroup("csv"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCsv))
}

// check for flag dependencies (one flag depends from another)
func checkFlagDependencies() error {
	if getopt.GetCount(flagRetriesWaitLong) > 0 && getopt.GetCount(flagRetriesLong) == 0 {
		return fmt.Errorf("\aoption '--%s' without  '--%s' option", flagRetriesWaitLong, flagRetriesLong)
	}
	return nil
}

// check if any string was provided to string options
func argProvided() error {
	for i := range flags {
		if flags[i].Seen() && !flags[i].IsFlag() && flags[i].Value().String() == "" {
			return fmt.Errorf("option '--%s' missing or wrong argument", flags[i].LongName())
		}
	}
	return nil
}

// check if long option are given with single or double dash
// without this check long option '--abcd', written with single dash, '-abcd', is parsed as a short option '-a'
// try with 'ls' command: 'ls -l -a' and 'ls -l --author' vs 'ls -l -author' (the latter behave as 'ls -l -a')
func checkLongOptionDashes(args []string) error {
	const dash byte = '-'
	const doubleDash string = "--"
	var wasBool bool = true

	// check if arg begin with dash (if begin with dash could be an option)
loopAllGivenArgs:
	for i := 0; i < len(args); i++ {
		var counter int = 0

		// check for empty strings, such as failed shell expansion - eg: "$(cat non-existant-file)"
		if args[i] == "" {
			continue
		}

		// if previous option was not bool, then next arg is (maybe) only the option argument
		if !wasBool {
			wasBool = true
			continue
		}

		// don't check arguments without dash prefix
		if len(args[i]) > 0 && args[i][0] != dash {
			continue
		}

		// don't check arguments with dash prefix + single char - eg: -a, -b, -c, etc...
		if len(args[i]) == 2 && args[i][0] == dash {
			continue
		}

		// remove single or double dash prefix
		s := strings.TrimPrefix(args[i], string(dash))
		s = strings.TrimPrefix(s, string(dash))

		// remove first '=' char, if any (eg. --my-opt=someData)
		s, _, _ = strings.Cut(s, "=")

		// check if string is a number
		if isNum(s) {
			continue
		}

		for _, v := range flags {
			if v.LongName() == s && (args[i][0] == dash && args[i][1] != dash) {
				// if 'longoption' exist, but was given without double dash (eg: --longoption OK, -longoption WRONG)
				return fmt.Errorf("long option with single dash: '-%s' must be '--%[1]s'", s)
			} else if v.LongName() == s {
				// if option is not boolean, then next arg is not an option
				if !v.IsFlag() {
					wasBool = false
				}
				// 'longoption' exist and was given correctly (eg: --longoption)
				continue loopAllGivenArgs
			} else {
				// 'longoption' not found in this inner loop cycle
				counter++
			}
		}

		// option not found
		if counter >= len(flags) {
			return fmt.Errorf("wrong option: '%s'", args[i])
		}
	}
	return nil
}

// used by presets - check if any parameter, such as temperature or model, was given.
// If yes it use command-line parameter setting, instead of preset parameter setting.
func countFlags(args []string, flag string) bool {
	for _, v := range args {
		flagsCounter[v]++
	}
	if _, ok := flagsCounter[flag]; ok {
		return true
	}
	return false
}

// check 'string type' option arguments for "-" char as first char of argument
func checkFlagTypeString() error {
	for i := range flags {
		if flags[i].Seen() && (flagsTypes[i] == "string" && strings.HasPrefix(flags[i].Value().String(), "-")) {
			return fmt.Errorf("\awrong parameter for %s: '%s'", flags[i].Name(), flags[i].Value().String())
		}
	}
	return nil
}

// check if string is a number, integer or float - false = NOT number, true = IS number
func isNum(s string) bool {
	if _, err := strconv.ParseInt(s, 10, 64); err == nil {
		return true
	}
	if _, err := strconv.ParseFloat(s, 64); err == nil {
		return true
	}
	if _, err := strconv.ParseComplex(s, 128); err == nil {
		return true
	}
	return false
}
