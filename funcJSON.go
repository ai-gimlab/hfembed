package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
)

// create payloads
func createPayloadJSON() (payloadJSON []byte, err error) {
	// function name to return if any error occur
	var funcName string = "createPayloadJSON"
	inputs = strings.Split(prompt, ",,")

	// configure model request
	payload := &requestEmbed{
		Inputs: inputs,
	}

	// create JSON payload
	payloadJSON, err = json.Marshal(payload)
	if err != nil {
		return nil, fmt.Errorf("func %q - error creating embeddings JSON payload: %w\n", funcName, err)
	}
	return payloadJSON, nil
}

// pretty print one line JSON
func prettyPrintJsonString(jsonString string) (formattedString string, err error) {
	// function name to return if any error occur
	var funcName string = "prettyPrintJsonString"

	var buf bytes.Buffer
	if err := json.Indent(&buf, []byte(jsonString), "", "  "); err != nil {
		return "", fmt.Errorf("func %q - error formatting JSON response: %w\n", funcName, err)
	}

	return buf.String(), nil
}

// check for valid JSON format
func isValidJSON(data string) error {
	var temp interface{}
	err := json.Unmarshal([]byte(data), &temp)
	return err
}
